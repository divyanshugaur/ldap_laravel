<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Adldap\Laravel\Facades\Adldap;

class UserController extends Controller
{
    //
	
	public function index()
	{

	    $search_data = Adldap::search()->where('cn','*')->get();
		$data1 = array(
		array('data1', 'data2'),
		array('data3', 'data4')
		);
		$data[] = ['First Name', 'Last Name','Email','Number','Department','Location','Designation'];

		foreach($search_data as $user_data)
		{
		  $data[] = array($user_data->cn[0],$user_data->sn[0],$user_data->mail[0],$user_data->telephonenumber[0],$user_data->ou[0],$user_data->postalAddress[0],$user_data->designation[0]);
		}
		\Excel::create('Filename', function($excel) use($data) {
			
		$excel->sheet('Sheetname', function($sheet) use($data) {
		  $sheet->fromArray($data, null, 'A1', false, false);

		});

		})->export('xls');
	}

}
